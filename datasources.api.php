<?php

/**
 * Defines one or more datasource inputs.
 *
 * Note: The ids should be valid PHP identifiers of max 32 characters.
 *
 * @return array
 *   An associative array of datasource inputs, keyed by a unique
 *   identifier and containing associative arrays with the following keys:
 *   - name: The datasource input translated name.
 *   - description [optional]: A translated string for description.
 *   - fields: an array of fields
 *   array(
 *     'field1' => t('Name field 1'),
 *     'field2' => array(
 *       'name' => t('Name field 2'),
 *     	 'type' => 'drupal_type', // like Field API, default is string
 *       // datasource supports 1 level of multi valued fields (list) for the input record.
 *       // so type can be a 'list<drupal_type>', in this case getRecord() must provide an array of values.
 *       // You must provide at least 1 udid field to be used as functional key.
 *     ),
 *     ...
 *   )
 *   - class: the name of the datasource engine class implementing DatasourceEngineInterface.
 * 
 * @see datasources_inputs_registry()
 * @see DatasourceEngineAbstract::importRecord()
 */
function hook_datasources_input() {
  $inputs['example_some_datasources_input'] = array(
    'name' => t('Some Datasource Input'),
    'class' => 'SomeDatasourceEngineClass',
    'fields' => array(
      'field1' => t('Name field 1'),
      'field2' => array(
         'name' => t('Name field 2'),
       	 'type' => 'drupal_type',
       ),
    ),
  );
  $inputs['example_other_datasources_input'] = array(
    /* stuff */
  );

  return $inputs;
}