<?php

/**
 * @file
 * Engine classes.
 */

/**
 * Interface defining the methods of datasource engine.
 */
interface DatasourceEngineInterface {
  
  /**
   * The method to import a record of the datasource input.
   * 
   * @return boolean
   *   - TRUE : import done
   *   - FALSE : no more import
   */
  public function importRecord();
  
  /**
   * Some post import record stuff.
   * 
   * @param array $record
   * @param array $context
   *  A "entity ids" keyed array of arrays with keys :
   *    - entity : the updated entity
   *    - fields : the updated fields
   *  
   */
  public function postImportRecord(array $record, array $context);  
  
  /**
   * Initialize import.
   * 
   * @return boolean
   *   - TRUE : import can begin.
   *   - FALSE : import cannot begin.
   */
  public function initImport();
  
  /**
   * Some cleanup stuff.
   */
  public function completeImport();
}

/**
 * Abstract datasource engine.
 */
abstract class DatasourceEngineAbstract implements DatasourceEngineInterface {

  const IMPORTED = 1;
  const NOT_IMPORTED = 0;
  const EOF = -1;
  
  public $input_id;
  
  public function __construct($input_id) {
    $this->input_id = $input_id;
  }

  public function initImport() {
    return TRUE;
  }
  
  public function completeImport() {
    // nothing
  }
  
  /**
   * Default importRecord().
   * Call getRecord() and try to import te datas matching with udid fields.
   * 
   * @see DatasourceEngineInterface::importRecord()
   * @see getRecord()
   * 
   * @return integer
   *   The status of the import.
   *   - self::IMPORTED : record imported
   *   - self::NOT_IMPORTED : record not imported
   *   - self::EOF : no more record
   */
  public function importRecord() {
    $record=$this->getRecord();
    
    if (!$record) return self::EOF;
    
    $input = datasources_input_info($this->input_id);

    $entities=array();
    
    if ($input['has_key']) {
    
      $ids = array();
      // we try to match existing entity in the specified order
      $flat_udid=NULL;
      foreach ($input['keys'] as $field => $key_info) {
        // use only some key
        if ($key_info['use']) {
          $res=udid_entity_ids($record[$field], array('entities' => $input['entity']));
          if (isset($res[$input['entity']])) {
            if (!$flat_udid) {
              // store the first udid for watchdog...
              $flat_udid="{$record[$field]['source']}/{$record[$field]['id']}";
            }
            $ids += $res[$input['entity']];
          }
        }
      }
  
      $n=0;
      foreach($ids as $id => $item) {
        $res=entity_load($input['entity'], array($id));
        if ($res) {
          list(, , $bundle)=entity_extract_ids($input['entity'], $res[$id]);
          if ($input['bundle']==$bundle || $input['policies']['bad_bundle'] = 'update') {
            // entity is updatable
            $entities[$id]=$res[$id];
            
            if ($input['policies']['duplicate']=='first') {
              // retrieve and update only the first entity
              break;
            }
            elseif ($n>0 && $input['policies']['duplicate']=='abort') {
              watchdog('datasource', t('Duplicate policy : a record from datasource input !input was not imported because it matches multiple entities (!udid).'), array('!input' => $this->input_id, '!udid' => $flat_udid), WATCHDOG_WARNING);
              return self::NOT_IMPORTED;
            }
            $n++;
          }
        }
      }
    
      if (!empty($entities) && !$input['policies']['update']) {
        watchdog('datasource', t('Update policy : a record from datasource input !input was not imported (!udid).'), array('!input' => $this->input_id, '!udid' => $flat_udid), WATCHDOG_WARNING);
        return self::NOT_IMPORTED;
      }
    }
    
    if (empty($entities) && !$input['policies']['insert']) {   
      watchdog('datasource', t('Insert policy : a record from datasource input !input was not imported (!udid).'), array('!input' => $this->input_id, '!udid' => $flat_udid), WATCHDOG_WARNING);
      return self::NOT_IMPORTED;
    }
    
    if (empty($entities)) {
      //create an empty entity
      $entity=entity_create_stub_entity($input['entity'], array(0, NULL, $input['bundle']));
      datasources_set_language($input['entity'], $entity);
      entity_save($input['entity'], $entity); // saving seams easier to handle id
      list($id, , ) = entity_extract_ids($input['entity'], $entity);      
      
      $entities[$id]=$entity;
    }
    
    $to_save=array();
    $to_save_context=array();
    foreach ($entities as $id => $entity) {
      $fields_info=datasources_entity_fields($this->input_id, $entity);
      foreach ($record as $field => $value) {
        if ($input['fields'][$field]['map']) {
          
          // try to get the underlaying entity
          $w=$fields_info['fields'][$input['fields'][$field]['map']]['wrapper'];
          do {
            $info = $w->info();
            if (isset($info['parent'])) {
              $w = $info['parent'];
            }
            else {
              $w = NULL;
              break;
            }
          } while(!$w instanceof EntityDrupalWrapper);
          
          if ($w) {
            
            // go it !
            $w->language(language_default('language'));
            $fields_info['fields'][$input['fields'][$field]['map']]['wrapper']->set($value);
          
            // store wrapper of updated entities
            if (!in_array($w, $to_save, TRUE)) {
              $to_save[]=$w;
            }
            
            // store some usefull (?) infos
            if (!isset($to_save_context[$id])) {
              $to_save_context[$id]=array(
                'entity' => $entity,
                'fields' => array(),
              );
            }
            $to_save_context[$id]['fields'][$field] = $input['fields'][$field]['map'];
          }
          else {
            // oops
            watchdog('datasource', t('Error : while importing datasource input field !input::!field, cannot find underlaying entity for on record (!udid).'), array('!input' => $this->input_id, '!field' => $field, '!udid' => $flat_udid), WATCHDOG_ERROR);
          }
        }
      }
    }
    
    // save underlaying entities using wrapper
    foreach ($to_save as $w) {
      $w->save();
    }
    
    // allow module to do more stuff
    $this->postImportRecord($record, $to_save_context);
        
  }
  
  public function postImportRecord(array $record, array $context) {
    // nothing by default
  }
  
  /**
   * Must provide an input record.
   * The input record is an array with values keyed with fields defined in the datasource input.
   * 
   * @see hook_datasources_input()
   */
  abstract protected function getRecord();
  
}

